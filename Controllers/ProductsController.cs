﻿using BasicWarehouse.Models;
using BasicWarehouse.Providers;
using BasicWarehouse.Utils;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BasicWarehouse.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {

        private readonly IDataAccessProvider _dataAccessProvider;

        public ProductsController(IDataAccessProvider dataAccessProvider)
        {
            _dataAccessProvider = dataAccessProvider;
        }

        // GET: api/<ValuesController>/hash
        [HttpGet("{hash}")]
        public IEnumerable<Product> GetProducts(string hash)
        {
            if (Hashkey.CheckHashkey(hash))
            {
                return _dataAccessProvider.GetProductRecords();
            }
            return null;
        }

        // GET api/<ValuesController>/id/hash
        [HttpGet("{id}/{hash}")]
        public Product GetProduct(int id, string hash)
        {
            if (Hashkey.CheckHashkey(hash))
            {
                return _dataAccessProvider.GetProductSingleRecord(id);
            }
            return null;
        }

        // POST api/<ValuesController>/hash
        [HttpPost("{hash}")]
        public IActionResult AddProduct([FromBody] Product product, string hash)
        {
            if (ModelState.IsValid && Hashkey.CheckHashkey(hash))
            {
                _dataAccessProvider.AddProductRecord(product);
                return Ok();
            }
            return BadRequest();
        }

        // PUT api/<ValuesController>/hash
        [HttpPut("{hash}")]
        public IActionResult UpdateProduct([FromBody] Product product, string hash)
        {
            if (ModelState.IsValid && Hashkey.CheckHashkey(hash)) 
            {
                _dataAccessProvider.UpdateProductRecord(product);
                return Ok();
            }
            return BadRequest();
        }

        // DELETE api/<ValuesController>/id/hash
        [HttpDelete("{id}/{hash}")]
        public IActionResult DeleteProduct(int id, string hash)
        {
            var product = _dataAccessProvider.GetProductSingleRecord(id);
            if (product != null && Hashkey.CheckHashkey(hash)) 
            {
                _dataAccessProvider.DeleteProductRecord(id);
                return Ok();
            }
            return BadRequest();
        }
    }
}

