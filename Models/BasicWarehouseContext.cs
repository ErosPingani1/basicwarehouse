using Microsoft.EntityFrameworkCore;

namespace BasicWarehouse.Models
{
    public class BasicWarehouseContext: DbContext
    {
        public BasicWarehouseContext(DbContextOptions<BasicWarehouseContext> options) : base(options) { }

        public DbSet<Product> Products { get; set; }

        /** Metodo per settare un unica primary key ed evitare errori eventuali */
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public override int SaveChanges()
        {
            ChangeTracker.DetectChanges();
            return base.SaveChanges();
        } 
    }
}
