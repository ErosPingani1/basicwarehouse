import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from 'src/app/models/product';
import { WarehouseService } from 'src/app/services/warehouse.service';

@Component({
    selector: 'app-edit-product-content',
    templateUrl: './edit-product-content.html',
    styleUrls: ['./edit-product-content.css']
})
export class EditProductContentComponent {

    @Input() product: Product;

    constructor(
        public activeModal: NgbActiveModal,
        public warehouseService: WarehouseService
    ) { }

    /** Metodo che genera un nuovo prodotto partendo dal precedente modificando i parametri con i valori nella form */
    private generateNewProduct(): Product {
        const newProd = this.product;
        newProd.name = (<HTMLInputElement>document.querySelector('#nameInput')).value;
        newProd.description = (<HTMLInputElement>document.querySelector('#descInput')).value;
        newProd.price = Number((<HTMLInputElement>document.querySelector('#priceInput')).value);
        newProd.quantity = Number((<HTMLInputElement>document.querySelector('#qtyInput')).value);
        return newProd;
    }

    /** Metodo che effettua l'update dei valori del prodotto, utilizzando il prodotto popolato con i nuovi valori */
    protected updateProductWithNewValues(): void {
        this.warehouseService.updateProduct(this.generateNewProduct()).subscribe({
            next: () => {
                this.activeModal.close();
            },
            error: err => alert('Si è verificato un errore, si prega di riprovare: ' + JSON.stringify(err))
        });
    }

}
