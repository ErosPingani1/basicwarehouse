import { Component, Injectable, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from 'src/app/models/product';
import { AddProductContentComponent } from '../add-product-content/add-product.component';
import { DeleteProductContentComponent } from '../delete-product-content/delete-product.component';
import { EditProductContentComponent } from '../edit-product-content/edit-product-content.component';

@Injectable()
@Component({
    selector: 'app-product-modal',
    templateUrl: './edit-product.html',
    styleUrls: ['./edit-product.css']
})
export class ProductActionModalComponent {

    @Input() product: Product;
    @Input() modalType: number;

    protected modalTypes = {
        1: {
            class: 'btn btn-primary',
            text: 'Modifica prodotto'
        },
        2: {
            class: 'btn btn-danger',
            text: 'Elimina prodotto'
        },
        3: {
            class: 'btn btn-primary',
            text: 'Aggiungi prodotto'
        }
    };

    constructor(
        public modalService: NgbModal
    ) { }

    protected openModal() {
        const modalRefs = this.modalService.open(this.modalType === 1
            ? EditProductContentComponent
            : this.modalType === 2
            ? DeleteProductContentComponent
            : AddProductContentComponent);
        if (this.product) {
            modalRefs.componentInstance.product = this.product;
        }
    }

}
