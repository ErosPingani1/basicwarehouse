import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from 'src/app/models/product';
import { WarehouseService } from 'src/app/services/warehouse.service';

@Component({
    selector: 'app-delete-product',
    templateUrl: './delete-product.html',
    styleUrls: ['./delete-product.css']
})
export class DeleteProductContentComponent {

    @Input() product: Product;

    constructor(
        public activeModal: NgbActiveModal,
        public warehouseService: WarehouseService
    ) { }

    /** Eliminazione di un prodotto con conseguente reload della pagina in caso di esito positivo */
    protected deleteProduct(): void {
        this.warehouseService.deleteProduct(this.product.id).subscribe({
            next: () => {
                window.location.reload();
            },
            error: err => alert('Si è verificato un errore, si prega di riprovare: ' + JSON.stringify(err))
        });
    }

}
