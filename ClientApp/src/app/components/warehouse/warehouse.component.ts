import { Component, Injectable } from '@angular/core';
import { Product } from 'src/app/models/product';

@Injectable()
@Component({
    selector: 'app-warehouse',
    templateUrl: './warehouse.component.html',
    styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent {

    protected selectedProduct: Product;

    constructor() { }

    /** Metodo che gestisce l'evento di output al click di un elemento della lista prodotti */
    protected manageSelectedProduct(product: Product): void {
        this.selectedProduct = product;
    }

}
