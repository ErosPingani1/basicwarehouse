import { Component, EventEmitter, Injectable, Output } from '@angular/core';
import { Product } from 'src/app/models/product';
import { WarehouseService } from 'src/app/services/warehouse.service';

/** Componente relativo alla visualizzazione della lista di elementi ottenuta dal DB */
@Injectable()
@Component({
    selector: 'app-warehouse-list',
    templateUrl: './warehouse-list.component.html',
    styleUrls: ['./warehouse-list.component.css']
})
export class WarehouseListComponent {

    protected loading = true;
    protected products: Product[];

    /** Evento di output utile all'invio del prodotto selezionato alla warehouse page */
    @Output() productSelected = new EventEmitter<Product>();

    constructor(
        private warehouseService: WarehouseService,
    ) {
        this.warehouseService.getProducts().subscribe({
            next: prods => {
                this.loading = false;
                this.products = prods;
            },
            error: err => alert('Si è verificato un errore, si prega di riprovare: ' + JSON.stringify(err))
        });
    }

    /** Metodo che emette l'evento di click su un prodotto della lista e ne invia il valore alla warehouse page */
    protected productClicked(product: Product): void {
        this.productSelected.emit(product);
    }

}
