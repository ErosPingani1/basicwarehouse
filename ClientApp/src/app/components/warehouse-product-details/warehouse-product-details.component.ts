import { Component, Injectable, Input } from '@angular/core';
import { Product } from 'src/app/models/product';
import { EditProductContentComponent } from '../edit-product-content/edit-product-content.component';

/** Componente relativo alla visualizzazione dei dettagli di uno specifico prodotto */
@Injectable()
@Component({
    selector: 'app-warehouse-product-details',
    templateUrl: './warehouse-product-details.html',
    styleUrls: ['./warehouse-product-details.css']
})
export class WarehouseProductDetailsComponent {

    @Input() product: Product;

    constructor() { }

}
