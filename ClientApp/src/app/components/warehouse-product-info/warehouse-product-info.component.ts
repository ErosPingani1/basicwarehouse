import { Component, Injectable, Input } from '@angular/core';

@Injectable()
@Component({
    selector: 'app-warehouse-product-info',
    templateUrl: './warehouse-product-info.html',
    styleUrls: ['./warehouse-product-info.css']
})
export class WarehouseProductInfoComponent {

    @Input() sku: string;
    @Input() price: number;
    @Input() qty: number;

    constructor() { }

}
