import { Component, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EventEmitter } from 'events';
import { Product } from 'src/app/models/product';
import { WarehouseService } from 'src/app/services/warehouse.service';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.html',
    styleUrls: ['./add-product.css']
})
export class AddProductContentComponent {

    constructor(
        public activeModal: NgbActiveModal,
        public warehouseService: WarehouseService
    ) { }

    private generateNewProduct(): Product {
        const newProd = new Product;
        newProd.name = (<HTMLInputElement>document.querySelector('#nameInput')).value;
        newProd.description = (<HTMLInputElement>document.querySelector('#descInput')).value;
        newProd.image = (<HTMLInputElement>document.querySelector('#imageInput')).value;
        newProd.sku = (<HTMLInputElement>document.querySelector('#skuInput')).value;
        newProd.price = Number((<HTMLInputElement>document.querySelector('#priceInput')).value);
        newProd.quantity = Number((<HTMLInputElement>document.querySelector('#qtyInput')).value);
        return newProd;
    }

    /** Aggiunta di un nuovo prodotto con reload della pagina in caso di esito positivo */
    protected addProduct(): void {
        this.warehouseService.addProduct(this.generateNewProduct()).subscribe({
            next: () => {
                window.location.reload();
            },
            error: err => alert('Si è verificato un errore, si prega di riprovare: ' + JSON.stringify(err))
        });
    }

}
