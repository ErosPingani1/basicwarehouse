import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { WarehouseComponent } from './components/warehouse/warehouse.component';
import { WarehouseService } from './services/warehouse.service';
import { WarehouseListComponent } from './components/warehouse-list/warehouse-list.component';
import { WarehouseProductDetailsComponent } from './components/warehouse-product-details/warehouse-product-details.component';
import { WarehouseProductInfoComponent } from './components/warehouse-product-info/warehouse-product-info.component';
import { ProductActionModalComponent } from './components/edit-product/edit-product.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditProductContentComponent } from './components/edit-product-content/edit-product-content.component';
import { DeleteProductContentComponent } from './components/delete-product-content/delete-product.component';
import { AddProductContentComponent } from './components/add-product-content/add-product.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    WarehouseComponent,
    WarehouseListComponent,
    WarehouseProductDetailsComponent,
    WarehouseProductInfoComponent,
    ProductActionModalComponent,
    EditProductContentComponent,
    DeleteProductContentComponent,
    AddProductContentComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    NgbModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/warehouse', pathMatch: 'full' },
      { path: 'warehouse', component: WarehouseComponent }
    ]),
  ],
  providers: [
    WarehouseService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    EditProductContentComponent,
    DeleteProductContentComponent,
    AddProductContentComponent
  ]
})
export class AppModule { }
