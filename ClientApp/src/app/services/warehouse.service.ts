import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product';

/** Elemento che effettua le chiamate al backend ASP.NET e ritorna ai vari component
 *  gli oggetti interessati
 */
@Injectable()
export class WarehouseService {

    private aspBE = 'http://localhost:25223/api/products/';

    private KEY = '5868e6e7b1b70636aac04176d097c4e98e27f7ebe60e330121854c63e05adaa4';

    private headers = new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
    });

    constructor(
        private http: HttpClient
    ) {  }

    /** Metodo che effettua la chiamata GetProductRecords per ottenere la lista di prodotti */
    public getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(this.aspBE + this.KEY, { headers: this.headers });
    }

    /** Metodo che effettua la chiamata UpdateProductRecord per aggiornare uno specifico prodotto */
    public updateProduct(product: Product): Observable<any> {
        return this.http.put(this.aspBE + this.KEY, JSON.stringify(product), { headers: this.headers });
    }

    /** Metodo che effettua la chiamata DeleteProductRecord per eliminare un prodotto tramite id */
    public deleteProduct(id: number): Observable<any> {
        return this.http.delete(this.aspBE + id + '/' + this.KEY, { headers: this.headers });
    }

    /** Metodo che aggiunge un nuovo prodotto al DB tramite la chiamata AddProduct */
    public addProduct(product: Product): Observable<Product> {
        return this.http.post<Product>(this.aspBE + this.KEY, JSON.stringify(product), { headers: this.headers });
    }

}
