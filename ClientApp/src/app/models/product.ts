export class Product {
    id: number;
    name: string;
    description: string;
    image: string;
    sku: string;
    price: number;
    quantity: number;
}
