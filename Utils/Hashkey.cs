namespace BasicWarehouse.Utils
{
    public class Hashkey 
    {
        private static string key = "5868e6e7b1b70636aac04176d097c4e98e27f7ebe60e330121854c63e05adaa4";

        public static bool CheckHashkey(string hash)
        {
            return hash == key;
        }
    }
}