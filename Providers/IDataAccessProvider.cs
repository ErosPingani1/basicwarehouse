using BasicWarehouse.Models;
using System.Collections.Generic;

namespace BasicWarehouse.Providers
{
    /** Interfaccia che definisce le operazioni CRUD da implementare nel DataAccessProvider */
    public interface IDataAccessProvider
    {
        void AddProductRecord(Product product);
        void UpdateProductRecord(Product product);
        void DeleteProductRecord(int id);
        Product GetProductSingleRecord(int id);
        List<Product> GetProductRecords();
    }
}
