using BasicWarehouse.Models;
using System.Collections.Generic;
using System.Linq;

namespace BasicWarehouse.Providers
{
    /** Un Provider in C# è l'equivalente di un repo in Angular, in cui sono definite le operazioni CRUD sul DB */
    public class DataAccessProvider: IDataAccessProvider
    {
        /** Il _context è l'entità relativa al DB collegato all'applicazione */
        private readonly BasicWarehouseContext _context;

        /** Costruttore del Provider nel quale viene definito il context */
        public DataAccessProvider(BasicWarehouseContext context) 
        {
            _context = context;
        }

        /** Metodo che aggiunge un record al DB (CREATE) */
        public void AddProductRecord(Product product) 
        {
            _context.Products.Add(product);
            _context.SaveChanges();
        }

        /** Modifica di un record del DB (UPDATE) */
        public void UpdateProductRecord(Product product)
        {
            _context.Products.Update(product);
            _context.SaveChanges();
        }

        /** Eliminazione di un record del DB (DELETE) */
        public void DeleteProductRecord(int id)
        {
            var entity = _context.Products.FirstOrDefault(t => t.Id == id);
            _context.Products.Remove(entity);
            _context.SaveChanges();
        }

        /** Lettura di un record del DB (READ) */
        public Product GetProductSingleRecord(int id)
        {
            return _context.Products.FirstOrDefault(t => t.Id == id);
        }

        /** Lettura dell'intera tabella del DB (READ) */
        public List<Product> GetProductRecords()
        {
            return _context.Products.ToList();
        }
    }
}
